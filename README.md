# Project Euler Py

## Description
[Project Euler](https://projecteuler.net) provides a series of challenging computer programming problems that will
require more than just mathematical insights to solve. In this repository I present solutions to some of these problems.

All solutions are implemented in Python 3.6.

## License
Provide under the [MIT License](LICENSE).
